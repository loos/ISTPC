# Post Hartree-Fock Methods

1. Introduction
  - Correlation energy
  - Notations
  - Outlines
 
2. Configuration Interaction (CI)
  - CI wave function ansatz
  - Excited determinants
  - Truncated CI and Pople diagram
  - Size of FCI space
  - CI energy and eigenvalue problem
  - Structure of the CI matrix
  - Slater-Condon rules
  - Size consistency
 
3. Perturbation Theory (PT)
  - Formal Rayleigh-Schrodinger PT
  - Expression of 2nd-order energy and 1st-order wave function
  - Moller-Plesset PT up to 2nd order (0th and 1st order)
  - Pople diagram and convergence
  - Computation of MP2 correlation energy (with AO-to-MO transformation)
 
4. Coupled Cluster (CC)
  - CC wave function ansatz
  - Wave operator, excitation operators, etc
  - CC vs CI: cluster analysis
  - Formal CC theory
  - CC energy
  - Pople diagram
  - Variational CC
  - Unitary CC
  - Similarity-transformed Hamiltonian
  - BCH expansion and amplitude/energy equations
  - Computation of CCD energy and amplitudes
  - MP2 guess and update of amplitudes
  
  
  
