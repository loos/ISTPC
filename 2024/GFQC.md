# Green's Functions in Quantum Chemistry

- Physical interpretation of the one- and two-body self-energy
- Lowdin partitioning technique
- Definition of the Green's function
- Dyson equation
- Matrix representation of G 
- Example for the HF Green's function
- Solving the Dyson equation (diagonal approximation, linearization, etc)
- Renormalization factor and spectral function
- Example of self-energy: the GW self-energy
- GW quasiparticle and satellites
- RPA problem
- Self-consistent procedure and approximations (G0W0, evGW, qsGW)
- Algorithm
